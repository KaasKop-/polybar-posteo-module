#!/usr/bin/env python
import imaplib

with imaplib.IMAP4_SSL("posteo.de") as posteo_server:
    login = posteo_server.login("username", "password")
    if login[0] == "OK":
        posteo_server.select(readonly=1)
        status, response = posteo_server.search(None, "(UNSEEN)")

        unread_message_count = 0
        for num in response[0].split():
            typ, data = posteo_server.fetch(num, "(RFC822)")
            # -1 because data[0] contains other misc data before the actual messages as well.
            unread_message_count = len(data[0]) - 1

        print(str(unread_message_count))
